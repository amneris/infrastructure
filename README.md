# infrastructure as code repository

The ONE and ONLY infrastructure as code repository for the ABA English organization.

## Goals

The goals of this repo are:
* Provide a single repository of the infrastructure of ABA
* Be able to make a disaster recovery from all the infrastructure and running services
* Provide a source of knowledge of the infrastructure with a self descriptive and organized code
