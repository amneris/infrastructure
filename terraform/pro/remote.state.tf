terraform {
  backend "s3" {
    bucket  = "terraform.abaenglish.com"
    key     = "pro.tfstate"
    region  = "eu-west-1"
  }
}

provider "aws" {
  region    = "eu-west-1"
}
