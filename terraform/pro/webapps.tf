module "vpc_abawebapps" {
  source          = "../modules/vpc"
  vpc_cidr        = "10.128.0.0/16"
  vpc_name        = "abawebapps"
  whitelist_cidr  = [ "91.126.242.146/32" ]
}

module "vpc_peering_abawebapps_analytics" {
  source          = "../modules/vpc_peering"
  peer_owner_id   = "065259791921"
  peer_vpc_id     = "vpc-409c3124" # Analytics VPC
  rt_id           = "${module.vpc_abawebapps.rt_pub_id}"
  vpc_id          = "${module.vpc_abawebapps.vpc_id}"
}
