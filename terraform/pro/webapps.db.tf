resource "aws_db_subnet_group" "subgrp_pub" {
  name        = "subgrp_pub-abawebapps"
  description = "Default public subnet group for abawebapps"
  subnet_ids  = ["${module.vpc_abawebapps.public_net_ids}"]

  lifecycle {
    create_before_destroy = true
  }

  tags {
    Name      = "subgrp_pub-abawebapps"
    Terraform = ""
  }
}

resource "aws_rds_cluster_parameter_group" "rds-cpg" {
  name        = "rds-cpg-aurora56-abawebapps"
  family      = "aurora5.6"
  description = "PG for ABA WebApps aurora cluster"

  parameter {
    name  = "time_zone"
    # As the is the same as Madrid,
    #   check http://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/Aurora.Overview.html#Aurora.Overview.LocalTimeZone
    value = "Europe/Paris"
  }

  lifecycle {
    create_before_destroy = true
  }

  tags {
    Name      = "rds-cpg-aurora5.6_abawebapps"
    Terraform = ""
  }
}

resource "aws_db_parameter_group" "rds-pg" {
  name        = "rds-pg-aurora56-abawebapps"
  family      = "aurora5.6"
  description = "PG for ABA WebApps aurora instances"

  parameter {
    name  = "general_log"
    value = 1
  }

  parameter {
    name  = "log_output"
    value = "FILE"
  }

  parameter {
    name  = "slow_query_log"
    value = 1
  }

  lifecycle {
    create_before_destroy = true
  }

  tags {
    Name      = "rds-pg-aurora5.6_abawebapps"
    Terraform = ""
  }
}

data "aws_vpc" "vpc_analytics" {
  id = "vpc-409c3124"
}

module "sg_dbcampus" {
  source        = "../modules/sg"
  name          = "dbcampus"
  description   = "RDS ABA WebApps SG"
  cidr          = [
    "${data.aws_vpc.vpc_analytics.cidr_block}",
    "52.213.28.234/32"  # New Relic monitoring
  ]
  sg_ids        = [ "${module.sg_webapps.id}" ]
  sg_ids_count  = 1
  from_port     = 3306
  to_port       = 3306
  protocol      = "tcp"
  vpc_id        = "${module.vpc_abawebapps.vpc_id}"
}

resource "aws_rds_cluster" "dbcampus-cluster" {
  cluster_identifier              = "rds-dbcampus-cluster"
  master_username                 = "abaadmin"
  master_password                 = "changeme" # this value is changed after creation
  availability_zones              = [ "${module.vpc_abawebapps.zones}" ]
  backup_retention_period         = 7
  db_cluster_parameter_group_name = "${aws_rds_cluster_parameter_group.rds-cpg.id}"
  db_subnet_group_name            = "${aws_db_subnet_group.subgrp_pub.id}"
  preferred_backup_window         = "04:00-04:30"
  preferred_maintenance_window    = "mon:05:00-mon:05:30"
#  replication_source_identifier   = "arn:aws:rds:eu-west-1:065259791921:db:dbcampus-slave-rds" # Comment after promote
  vpc_security_group_ids          = [
    "${module.vpc_abawebapps.sg_whitelist_id}",
    "${module.sg_dbcampus.id}"
  ]
  apply_immediately             = true

  lifecycle {
    create_before_destroy = true
    prevent_destroy       = true
  }
}

resource "aws_rds_cluster_instance" "dbcampus" {
  count                   = 2
  identifier              = "rds-dbcampus-${count.index + 1}"
  cluster_identifier      = "${aws_rds_cluster.dbcampus-cluster.id}"
  db_parameter_group_name = "${aws_db_parameter_group.rds-pg.id}"
  db_subnet_group_name    = "${aws_db_subnet_group.subgrp_pub.id}"
  instance_class          = "db.r3.xlarge"
  monitoring_interval     = 60
  monitoring_role_arn     = "arn:aws:iam::065259791921:role/rds-monitoring-role" # already exists
  promotion_tier          = "${count.index}"
  publicly_accessible     = true

  lifecycle {
    create_before_destroy = true
    prevent_destroy       = true
  }

  tags {
    Name      = "rds-dbcampus-${count.index + 1}"
    Terraform = ""
  }
}
