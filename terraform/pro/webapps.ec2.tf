# HAproxy + ABA Webapps
module "sg_haproxy" {
  source        = "../modules/sg_http"
  name          = "haproxy"
  description   = "HAProxy SG"
  sg_ids        = [ "${module.sg_webapps_lb.id}" ]
  sg_ids_count  = 1
  vpc_id        = "${module.vpc_abawebapps.vpc_id}"
}

resource "aws_security_group_rule" "sg_haproxy_healthcheck_rule" {
  type                      = "ingress"
  from_port                 = "${module.lb_webapps.check_port}"
  to_port                   = "${module.lb_webapps.check_port}"
  protocol                  = "tcp"
  source_security_group_id  = "${module.sg_webapps_lb.id}"
  security_group_id         = "${module.sg_haproxy.id}"

  lifecycle {
    create_before_destroy = true
  }
}

module "ec2_haproxy" {
  source      = "../modules/ec2"
  instances   = 2
  service     = "haproxy"
  prefix      = "pro"
  key_name    = "production"
  monitoring  = true
  subnets     = ["${module.vpc_abawebapps.public_net_ids}"]
  sg_ids      = ["${module.vpc_abawebapps.sg_whitelist_id}", "${module.sg_haproxy.id}"]
  type        = "t2.micro"
}

module "sg_webapps" {
  source        = "../modules/sg_http"
  name          = "webapps"
  description   = "ABA WebApps SG"
  sg_ids        = [ "${module.sg_haproxy.id}" ]
  sg_ids_count  = 1
  vpc_id        = "${module.vpc_abawebapps.vpc_id}"
}

module "ec2_webapps" {
  source          = "../modules/ec2"
  instances       = 5
  service         = "webapps"
  prefix          = "pro"
  ebs_optimized   = true
  key_name        = "production"
  monitoring      = true
  subnets         = ["${module.vpc_abawebapps.public_net_ids}"]
  subnets_offset  = 2
  sg_ids          = ["${module.vpc_abawebapps.sg_whitelist_id}", "${module.sg_webapps.id}"]
  type            = "m4.large"
}
