# ABA Webapps
module "sg_webapps_lb" {
  source      = "../modules/sg_https"
  name        = "webapps_lb"
  description = "ABA WebApps LB SG"
  cidr        = [ "0.0.0.0/0" ]
  vpc_id      = "${module.vpc_abawebapps.vpc_id}"
}

module "lb_webapps" {
  source            = "../modules/alb_https"
  name              = "pro-webapps"
  certificate_arn   = "arn:aws:acm:eu-west-1:065259791921:certificate/a37fd1ff-52d6-4c80-9afb-a51ccd9c3fb4"
  check_path        = "/health"
  check_port        = 1936
  check_status_code = 401
  target_ids        = [ "${module.ec2_haproxy.ids}" ]
  target_ids_count  = "${module.ec2_haproxy.instance_count}"
  sg_ids            = [ "${module.sg_webapps_lb.id}" ]
  subnets           = [ "${module.vpc_abawebapps.public_net_ids}" ]
  vpc_id            = "${module.vpc_abawebapps.vpc_id}"
}
