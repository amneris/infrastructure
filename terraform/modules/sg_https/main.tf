module "sg" {
  source        = "../sg_http"
  name          = "${var.name}"
  description   = "${var.description}"
  cidr          = "${var.cidr}"
  sg_ids        = "${var.sg_ids}"
  sg_ids_count  = "${var.sg_ids_count}"
  vpc_id        = "${var.vpc_id}"
}

resource "aws_security_group_rule" "sg_cidr" {
  count             = "${length(var.cidr)}"
  type              = "ingress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = [ "${var.cidr[count.index]}" ]
  security_group_id = "${module.sg.id}"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_security_group_rule" "sg_ids" {
  count                     = "${length(var.sg_ids)}"
  type                      = "ingress"
  from_port                 = 443
  to_port                   = 443
  protocol                  = "tcp"
  source_security_group_id  = "${var.sg_ids[count.index]}"
  security_group_id         = "${module.sg.id}"

  lifecycle {
    create_before_destroy = true
  }
}

output "id" {
  value = "${module.sg.id}"
}
