module "sg" {
  source        = "../sg"
  name          = "${var.name}"
  description   = "${var.description}"
  cidr          = [ "${var.cidr}" ]
  sg_ids        = "${var.sg_ids}"
  sg_ids_count  = "${var.sg_ids_count}"
  from_port     = 80
  to_port       = 80
  protocol      = "tcp"
  vpc_id        = "${var.vpc_id}"
}

output "id" {
  value = "${module.sg.id}"
}
