resource "aws_alb" "alb" {
  name            = "${var.name}-lb"
  security_groups = [ "${var.sg_ids}" ]
  subnets         = [ "${var.subnets}" ]

  lifecycle {
    create_before_destroy = true
  }

  tags {
    Name      = "${var.name}-lb"
    Terraform = ""
  }
}

output "dns" {
  value = "${aws_alb.alb.dns_name}"
}

resource "aws_alb_target_group" "alb_target" {
  name      = "${var.name}-target"
  port      = "${var.target_port}"
  protocol  = "${var.target_protocol}"
  vpc_id    = "${var.vpc_id}"

  health_check {
    interval            = 5
    path                = "${var.check_path}"
    port                = "${var.check_port}"
    protocol            = "${var.check_protocol}"
    matcher             = "${var.check_status_code}"
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 4
  }

  lifecycle {
    create_before_destroy = true
  }

  tags {
    Name      = "${var.name}-target"
    Terraform = ""
  }
}

resource "aws_alb_listener" "http_listener" {
  load_balancer_arn = "${aws_alb.alb.arn}"
  port              = 80

  default_action {
    target_group_arn  = "${aws_alb_target_group.alb_target.arn}"
    type              = "forward"
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_alb_listener" "https_listener" {
  certificate_arn   = "${var.certificate_arn}"
  load_balancer_arn = "${aws_alb.alb.arn}"
  port              = 443
  protocol          = "HTTPS"

  default_action {
    target_group_arn  = "${aws_alb_target_group.alb_target.arn}"
    type              = "forward"
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_alb_target_group_attachment" "target_attachment" {
  count             = "${var.target_ids_count}"
  target_group_arn  = "${aws_alb_target_group.alb_target.arn}"
  target_id         = "${var.target_ids[count.index]}"
  port              = "${var.target_port}"
}
