variable "certificate_arn"  {}
variable "name"             {}
variable "target_ids_count" {}
variable "vpc_id"           {}

variable "check_path" {
  default = "/"
}

variable "check_port" {
  default = 80
}

variable "check_protocol" {
  default = "HTTP"
}

variable "check_status_code" {
  default = 200
}

variable "sg_ids" {
  type = "list"
}

variable "subnets" {
  type = "list"
}

variable "target_ids" {
  description = "IDs of target instances"
  type        = "list"
}

variable "target_port" {
  default = 80
}

variable "target_protocol" {
  default = "HTTP"
}

output "check_port" {
  value = "${var.check_port}"
}
