variable "cidr" {
  default     = []
  description = "List of CIDR included in SG"
  type        = "list"
}

variable "sg_ids" {
  default     = []
  description = "List of SG ids included in SG"
  type        = "list"
}

variable "sg_ids_count" {
  # Fixes 'value of 'count' cannot be computed'
  default = 0
}

variable "from_port" {
  default = 0
}

variable "protocol" {
  default = "-1"
}

variable "to_port" {
  default = 0
}

variable "description"  {}
variable "name"         {}
variable "vpc_id"       {}
