resource "aws_security_group" "sg" {
  name        = "sg_${var.name}"
  description = "${var.description}"
  vpc_id      = "${var.vpc_id}"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
  }

  tags {
    Name      = "sg_${var.name}"
    Terraform = ""
  }
}

resource "aws_security_group_rule" "sg_cidr" {
  count             = "${length(var.cidr)}"
  type              = "ingress"
  from_port         = "${var.from_port}"
  to_port           = "${var.to_port}"
  protocol          = "${var.protocol}"
  cidr_blocks       = [ "${var.cidr[count.index]}" ]
  security_group_id = "${aws_security_group.sg.id}"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_security_group_rule" "sg_ids" {
#  count                     = "${length(var.sg_ids)}"
  count                     = "${var.sg_ids_count}"
  type                      = "ingress"
  from_port                 = "${var.from_port}"
  to_port                   = "${var.to_port}"
  protocol                  = "${var.protocol}"
  source_security_group_id  = "${var.sg_ids[count.index]}"
  security_group_id         = "${aws_security_group.sg.id}"

  lifecycle {
    create_before_destroy = true
  }
}

output "id" {
  value = "${aws_security_group.sg.id}"
}
