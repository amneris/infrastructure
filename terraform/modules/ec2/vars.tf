variable "key_name" {}

variable "ami_filter_name" {
  default     = "CentOS Linux 7 x86_64 HVM EBS*"
}

variable "ebs_optimized" {
  default     = false
}

variable "instances" {
  default     = 1
  description = "Number of instances to launch"
}

variable "monitoring" {
  default     = false
}

variable "prefix" {
  description = "Instance's prefix name"
}

variable "rootfs_size" {
  default     = 8
  description = "Size of root block storage"
}

variable "service" {
  description = "Application service name, i.e. haproxy, webapps, graylog, etc."
}

variable "sg_ids" {
  description = "List of SG to attach to instance"
  type        = "list"
}

variable "subnets" {
  description = "List of subnet ids to start instances in"
  type        = "list"
}

variable "subnets_offset" {
  default     = 0
  description = "Offset to apply to the list of subnets when creating instances"
}

variable "type" {
  description = "EC2 instance type (t2.small, etc.)"
  default     = "t2.small"
}
