data "aws_ami" "ami" {
  most_recent = true

  filter {
    name   = "name"
    values = ["${var.ami_filter_name}"]
  }

  owners = ["679593333241"] # AWS Marketplace
}

resource "aws_instance" "instance" {
  count                   = "${var.instances}"
  ami                     = "${data.aws_ami.ami.id}"
  ebs_optimized           = "${var.ebs_optimized}"
  instance_type           = "${var.type}"
  key_name                = "${var.key_name}"
  monitoring              = "${var.monitoring}"
  subnet_id               = "${var.subnets[(count.index + var.subnets_offset) % length(var.subnets)]}"
  vpc_security_group_ids  = ["${var.sg_ids}"]

  root_block_device {
    volume_type           = "gp2"
    volume_size           = "${var.rootfs_size}"
    delete_on_termination = true
  }

  lifecycle {
    create_before_destroy = true
  }

  tags {
    Name      = "${var.prefix}-${var.service}-${count.index + 1}"
    Service   = "${var.service}"
    Terraform = ""
  }
}

output "ids" {
  value = [ "${aws_instance.instance.*.id}" ]
}

output "instance_count" {
  value = "${var.instances}"
}
