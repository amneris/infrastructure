variable vpc_cidr {
  default = "10.0.0.0/16"
}

variable vpc_subnet_bits {
  default = 5
}

variable vpc_name {}

variable whitelist_cidr {
  default     = []
  description = "List of whitelisted CIDR included in 'default' SG"
  type        = "list"
}
