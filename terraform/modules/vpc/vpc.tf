resource "aws_vpc" "vpc" {
  cidr_block            = "${var.vpc_cidr}"
  enable_dns_hostnames  = true

  lifecycle {
    create_before_destroy = true
  }

  tags {
    Name      = "vpc_${var.vpc_name}"
    Terraform = ""
  }
}

output "vpc_id" {
  value = "${aws_vpc.vpc.id}"
}
