resource "aws_subnet" "subnets" {
  count                   = "${length(var.zones)}"
  availability_zone       = "${var.zones[count.index]}"
  cidr_block              = "${cidrsubnet(var.cidr, var.subnet_bits, count.index)}"
  map_public_ip_on_launch = "${var.map_public_ip}"
  vpc_id                  = "${var.vpc_id}"

  lifecycle {
    create_before_destroy = true
  }

  tags {
    Name      = "sub_${var.name}-${var.zones[count.index]}"
    Terraform = ""
  }
}

output "subnet_ids" {
  value = ["${aws_subnet.subnets.*.id}"]
}
