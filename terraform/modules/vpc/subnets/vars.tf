variable "cidr" {
  description = "Common cidr of subnets"
}

variable "map_public_ip" {
  default = false
}

variable "name" {
  description = "Common name of subnets"
}

variable "subnet_bits" {
  description = "Extra bits added to cidr netmask for each subnet"
}

variable "vpc_id" {}

variable "zones" {
  description = "List of availability zone names"
  type        = "list"
}
