module "whitelist" {
  source      = "../sg"
  name        = "whitelist_${var.vpc_name}"
  description = "SG to enable access from general known resources (i.e. ABA office, Jenkins, etc.)"
  cidr        = "${var.whitelist_cidr}"
  vpc_id      = "${aws_vpc.vpc.id}"
}

output "sg_whitelist_id" {
  value = "${module.whitelist.id}"
}
