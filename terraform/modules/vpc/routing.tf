resource "aws_internet_gateway" "igw" {
  vpc_id  = "${aws_vpc.vpc.id}"

  lifecycle {
    create_before_destroy = true
  }

  tags {
    Name      = "igw_${var.vpc_name}"
    Terraform = ""
  }
}

resource "aws_route_table" "rt_pub" {
  vpc_id = "${aws_vpc.vpc.id}"

  lifecycle {
    create_before_destroy = true
  }

  tags {
    Name      = "rt-pub_${var.vpc_name}"
    Terraform = ""
  }
}

resource "aws_route" "route_igw" {
  route_table_id          = "${aws_route_table.rt_pub.id}"
  destination_cidr_block  = "0.0.0.0/0"
  gateway_id              = "${aws_internet_gateway.igw.id}"

  lifecycle {
    create_before_destroy = true
  }
}


output "rt_pub_id" {
  value = "${aws_route_table.rt_pub.id}"
}

resource "aws_route_table_association" "rt_pub_assoc" {
  count           = "${length(module.public_nets.subnet_ids)}"
  route_table_id  = "${aws_route_table.rt_pub.id}"
  subnet_id       = "${module.public_nets.subnet_ids[count.index]}"
}
