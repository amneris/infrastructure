data "aws_availability_zones" "zones" {}

output "zones" {
  value = "${data.aws_availability_zones.zones.names}"
}

module "public_nets" {
  source        = "./subnets"
  cidr          = "${cidrsubnet(var.vpc_cidr, 1, 0)}"
  name          = "pub-${var.vpc_name}"
  map_public_ip = true
  subnet_bits   = "${var.vpc_subnet_bits - 1}"
  vpc_id        = "${aws_vpc.vpc.id}"
  zones         = "${data.aws_availability_zones.zones.names}"
}

output "public_net_ids" {
  value = "${module.public_nets.subnet_ids}"
}

### De we need them?
##module "private_nets" {
##  source        = "./subnets"
##  cidr          = "${cidrsubnet(var.vpc_cidr, 1, 1)}"
##  name          = "prv-${var.vpc_name}"
##  map_public_ip = false
##  subnet_bits   = "${var.vpc_subnet_bits - 1}"
##  vpc_id        = "${aws_vpc.vpc.id}"
##  zones         = "${data.aws_availability_zones.zones.names}"
##}
##
##output "private_net_ids" {
##  value = "${module.private_nets.subnet_ids}"
##}
