variable peer_owner_id  {}
variable peer_vpc_id    {}

variable rt_id {
  description = "Route table ID to attach peer route"
}

variable vpc_id {}
