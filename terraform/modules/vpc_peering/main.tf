# Pre: Both VPC are from the same account
data "aws_vpc" "vpc" {
  id  = "${var.vpc_id}"
}

data "aws_vpc" "peer_vpc" {
  id  = "${var.peer_vpc_id}"
}

resource "aws_vpc_peering_connection" "peering" {
  peer_owner_id                   = "${var.peer_owner_id}"
  peer_vpc_id                     = "${var.peer_vpc_id}"
  vpc_id                          = "${var.vpc_id}"
  auto_accept                     = true

  accepter {
    allow_remote_vpc_dns_resolution = true
  }

  requester {
    allow_remote_vpc_dns_resolution = true
  }

  lifecycle {
    create_before_destroy = true
  }

  tags {
    Name      = "vpc-peer_${data.aws_vpc.vpc.tags["Name"]}-${data.aws_vpc.peer_vpc.tags["Name"]}"
    Terraform = ""
  }
}

resource "aws_route" "route_peer" {
  route_table_id            = "${var.rt_id}"
  destination_cidr_block    = "${data.aws_vpc.peer_vpc.cidr_block}"
  vpc_peering_connection_id = "${aws_vpc_peering_connection.peering.id}"

  lifecycle {
    create_before_destroy = true
  }
}
