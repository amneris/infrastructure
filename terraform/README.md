# Terraform files #

This folder contains the terraform files used to create (orchestrate) infrastructure in AWS.

  WARNING
  This is a prototype of the terraform IAC, so be careful working with it. Check [Current status](#current-status).

## Current status ##

Currently, only ABA WebApp VPC is modeled with terraform in 'pro' environment (check its folder). As it was introduced to fully migrate to AWS under a deadline, it lacks of a naming and structure convention. Any improvement or suggestion is welcome.

### Next steps ###

As this time, the following steps are considered:

- Include instances not currently modeled in 'pro' environment: graylog, rundeck, etc.
- Replicate 'pro' environment in 'dev'
- Make an extensive use of variables in order to make the (future) modeled environments as generic as possible
- Define a naming convention
- Define a workflow for modelling with terraform

## Requirements ##

* Terraform == 0.9.3 (download from https://releases.hashicorp.com/terraform/)
* AWS cli configured to access abenglish account

To start working in a new environment, enter in its folder and execute:

```terraform init```
