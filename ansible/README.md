# Ansible config #

This folder contains the infrastructure configuration (EC2, ELB, etc.) deployed with Ansible.

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Requirements](#requirements)
- [Conventions](#conventions)
  - [Ansible best practices](#ansible-best-practices)
  - [Tags](#tags)
  - [Variables](#variables)
  - [SSH keys](#ssh-keys)
  - [Directory Layout](#directory-layout)
- [AWS account's config](#aws-accounts-config)
  - [Vault variables](#vault-variables)
  - [Dynamic inventory](#dynamic-inventory)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Requirements ##

In order to run these playbooks, you need *ansible* and *awscli* installed:

* Ansible (== 2.2.\*)

    > If you are wishing to run the latest released version of Ansible and you are running Red Hat Enterprise Linux (TM), CentOS, Fedora, Debian, or Ubuntu, we recommend using the OS package manager.
    > For other installation options, we recommend installing via “pip”, which is the Python package manager, though other options are also available.

    > ⚠ [**Latest Releases on Mac OSX**](https://docs.ansible.com/ansible/intro_installation.html#latest-releases-on-mac-osx) : The preferred way to install ansible on a Mac is via pip.
    * on macOS make sure you have *pip* and the [Command Line Tools Package](https://developer.apple.com/library/content/technotes/tn2339/_index.html) installed and updated
    ```
        xcode-select --install
        sudo python -m ensurepip
        sudo pip install -U pip
    ```
    * then:

    ```
    sudo pip install "ansible<2.3"
    ```

* [AWS Command Line Interface](https://aws.amazon.com/cli/) >= 1.6.1 with working ABA Land's and ABAEnglish' AWS keys for develop and production, respectively
    - Mac OSX with HomeBrew ```$ brew install awscli```
    - OR with PIP: ```$ pip install awscli```
    - ask for the AWS credentials to a fellow developer and set them up running
    ```
    aws configure
    ```
    - Check [AWS account's config](#aws-accounts-config) to see where are the credentials are used

* boto python library for use ec2.py
    ```
    sudo pip install --ignore-installed six boto
    ```

## Conventions ##

### Ansible best practices ###

We try and encourage to follow Ansible's Best Practices. So, when in doubt, check the following links:

* [Ansible Best Practices: The Essentials](https://www.ansible.com/blog/ansible-best-practices-essentials)
* [Ansible official Best Practices](https://docs.ansible.com/ansible/playbooks_best_practices.html)

Please, let us know if there is any violation or fix it and make a PR!

### Tags ###

In order to be enable to execute tasks in a fine grained manner, we should follow these rules:

* Tag tasks as upper in the scope as possible; e.g.:tag role call with its own name or when including file with the same tag, tag the include task, not everyone within its file
* As still is not possible (see [#11185](https://github.com/ansible/ansible/issues/11185)) to select tasks with all ANDed tags provided, provide repeatable tasks with tags like \<general tag\>-\<specific tag\>, e.g.: `apache` and `apache-ssl`

Also, use of the following tags is encouraged in the following conditions:

* **config**: when task may be called to perform a config update
* **deploy**: when task is deploying an ABA app
* **install**: when task should be performed as one off

### Variables ###

* Always put the namespace as a prefix in order to be able to override specific vars
* Provide default variable values and examples in roles, and try to not put specific ABA variables (we may want to open source it in the future)
* Override and group vars in group_vars and host_vars (check [Ansible variable inheritance](http://docs.ansible.com/ansible/playbooks_variables.html#variable-precedence-where-should-i-put-a-variable))
* Never put specific environment vars in inventories, use environment group_vars
* Try to use self descriptive variable names

### SSH keys ###

SSH keys shouldn't be referenced from inventories or playbooks, as exist more filesystem agnostic methods.

Instead, use `ssh-agent` and the command `ssh-add key.pem` before running a playbook. You can check your loaded keys with `ssh-add -l`.

### Directory Layout ###

An overview of directory layout:

```
.
|-- ansible.cfg
|-- develop.inv/            # Develop AWS account (ABA land)
|   |-- ec2.ini             # Config file for AWS EC2 discovery
|   |-- ec2.py              # AWS EC2 discovery script
|   |-- group_vars          # Symlink to ../group_vars
|   `-- static              # Static organization of managed EC2 instances
|-- production.inv/         # Production AWS account (ABAEnglish)
|   |-- ec2.ini
|   |-- ec2.py
|   |-- group_vars
|   `-- static
|-- nexica.inv
|-- group_vars/
|   |-- all/                # Default group_vars
|   |   |-- env.yml         # Vars defining which environment is being used
|   |   |-- dns.yml         # Category default vars
|   |   |-- newrelic.yml    # "        "       "
|   |   `-- webapps.yml     # "        "       "
|   |-- develop/            # Develop overriden vars
|   |   |-- dns.yml         # Override category vars
|   |   |-- main.yml        # "        "        "
|   |   |-- webapps.vault   # "        "        "   ; encrypted ones
|   |   `-- webapps.yml     # "        "        "
|   |-- local/
|   |-- nexica/
|   |-- oc/
|   `-- production/
|-- local.inv
|-- playbooks/              # All playbooks
|   |-- common.yml
|   |-- dns.yml
|   .
|   .
|   .
|   `-- webapps-rollback.yml
|-- README.md
|-- roles/                  # All roles
|   |-- ansistrano-deploy/
|   |-- common/
|   |-- dns/                # Standard role structure
|   |   |-- defaults/
|   |   |   `-- main.yml
|   |   |-- files/
|   |   |-- handlers/
|   |   |   `-- main.yml
|   |   |-- tasks/
|   |   |   `-- main.yml
|   |   `-- templates/
|   |       |-- named.conf.j2
|   |       `-- zonefile.j2
|   |-- docker/
.
.
.
|-- vault.env/              # Vault (i.e. secrets) environments
|   |-- develop/
|   |   `-- vault_pass      # Encrypted AWS KMS file containing environment vault passphrase
|   |-- production/
|   |   `-- vault_pass
|   `-- setenv              # Environment file sourced by .vault_pass script
`-- .vault_pass             # vault script to decrypt KMS file
```

## AWS account's config ##

In order to use some features that are connected with Amazon, yo need to get properly configured your access to the ABA's AWS accounts.

If you have different profiles, you can provide the `AWS_PROFILE=profile_name` environment to select it.

The features connected to AWS are the following ones:

### Vault variables ###

As some variables are passwords that need to be protected, they are encrypted with ansible-vault. In order to use it, define the environment that you want to use (check `vault.env` folders), set the following environment variable accordingly and source the `vault.env/setenv` file:

```
export VAULT_ENV=develop
. vault.env/setenv
```

This use the key file in `vault.env/<environment>/vault_pass`, that is in turn encrypted with AWS KMS in order to provide access control management to it.

### Dynamic inventory ###

Develop and production environment use dynamic inventory to discover AWS instances.
