---
- name: Install Apache 2.4
  yum:
    name: httpd-2.4*
    state: present
  tags:
    - install

- name: Ensure sites exists
  file:
    path: /etc/httpd/conf/sites/
    state: directory
    owner: root
    group: root
    mode: 0755
  tags:
    - install

- name: Copy httpd.conf file
  template:
    src: httpd.conf
    dest: /etc/httpd/conf/httpd.conf
    owner: root
    group: root
    mode: 0644
  notify: reload apache
  tags:
    - config

- name: Copy virtualhosts
  template:
    src: "sites/{{ item }}"
    dest: /etc/httpd/conf/sites/
    owner: root
    group: root
    mode: 0644
  with_items:
    - 00-monitor.conf
    - 80-api.conf
    - 80-campus.conf
    - 80-corporate.conf
    - 80-intranet.conf
    - 80-trx.conf
  notify: reload apache
  tags:
    - config
    - apache-sites

- name: Copy Environment vars
  template:
    src: env_vars.conf.j2
    dest: /etc/httpd/conf.d/env_vars.conf
    owner: root
    group: root
    mode: 0640
  notify: reload apache
  tags:
    - config
    - apache-env

- name: Ensure /var/www exists
  file:
    path: /var/www
    state: directory
    owner: root
    group: wwwuser
    mode: 0775
  tags:
    - install

- name: Ensure /var/log/abawebapps exists
  file:
    path: /var/log/abawebapps
    state: directory
    owner: apache
    group: root
    mode: 0777
  tags:
    - install

- name: Remove cgi and vhosts folders
  file:
    path: "/var/www/{{ item }}"
    state: absent
  with_items:
    - cgi-bin
    - vhosts
  tags:
    - install

- name: Remove Apache Modules unused
  file:
    path: "/etc/httpd/{{ item }}"
    state: absent
  with_items:
    - conf.modules.d/00-dav.conf
    - conf.modules.d/00-lua.conf
    - conf.modules.d/00-proxy.conf
    - conf.modules.d/00-ssl.conf
    - conf.modules.d/01-cgi.conf
  notify: reload apache
  tags:
    - install

- name: Create /var/www/passwd
  file:
    path: /var/www/passwd
    state: directory
    owner: wwwuser
    group: wwwuser
    mode: 0755
  tags:
    - install

- name: Copy htpasswd to control Intranet access
  copy:
    src: htpasswd
    dest: /var/www/passwd/htpasswd
    owner: wwwuser
    group: wwwuser
    mode: 0644
  when: is_production
  tags:
    - config

- name: Enable httpd_unified selinux boolean true
  seboolean:
    name: httpd_unified
    state: yes
    persistent: yes
  when: ansible_selinux.status|default() == 'enabled'
  tags:
    - install

- name: Enable httpd_can_network_connect_db selinux boolean true
  seboolean:
    name: httpd_can_network_connect_db
    state: yes
    persistent: yes
  when: ansible_selinux.status|default() == 'enabled'
  tags:
    - install

- name: Disable selinux in develop
  selinux:
    policy: targeted
    state: permissive
  when: is_develop and ansible_selinux.status|default() == 'enabled'
  tags:
    - install

- name: Install perl module required by logger script
  yum:
    name: perl-Sys-Syslog
    state: present
  tags:
    - install

- name: Copy logger script
  copy:
    src: logger_long.pl
    dest: /usr/local/bin/
    owner: root
    group: root
    mode: 0755
  tags:
    - install

- name: Ensure Apache is enabled
  service:
    name: httpd
    enabled: yes

- name: Ensure Apache is running
  service:
    name: httpd
    state: started
