#!/usr/bin/perl

use warnings;
use strict;

use Sys::Syslog qw (:DEFAULT setlogsock);
use Getopt::Std;

#-p local5.info -t loc-atrapalo.com-clones.log

my ($tag, $facility, $priority);

# parse options
my %opts;
getopts('t:p:', \%opts);

if (exists($opts{t})) {
  $tag = $opts{t};
}

if (exists($opts{p})) {
  ($facility, $priority) = split(/\./, $opts{p});
}

setlogsock('unix');

# open our log socket
openlog($tag, undef, $facility);

# log all our input
while (<STDIN>) {
  syslog($priority, $_);
}

# close the log socket
closelog;
