# config-manager
ABA English Ansible Provisioning Repository

### Estructura

Toda la estructura del proyecto de configuración automática está en un repositorio de github:
https://github.com/abaenglish/config-manager/

El proyecto se ha clonado en el servidor "logger" dentro de la home del usuario "aba".

Dentro del proyecto encontraremos lo siguiente:

  - (fichero) README.md : Corresponde a este documento
  - (fichero) production : Es el catálogo donde están registrados todos los servidores colgando de su rol/roles
  - (fichero) Ficheros .yml de la raíz del proyecto: Son los playbooks que corresponden a cada uno de los roles
  - (directorio) roles : Contiene cada uno de los roles con la definición de las tareas a realizar
    - Cada rol contiene tres subdirectorios:
       - (directorio) files : Son ficheros que se copiarán al nodo destino
       - (directorio) handlers : Son acciones que se realizan dependiendo de cada tarea (reloads, restarts)
       - (directorio) tasks : Son las tareas que se ejecutarán por orden lineal, main.yml puede incluir otros ficheros yml

 - Existe un directorio llamado /home/aba/files que contiene ficheros sensibles que no se recomienda subir a github

### Setup Pre-Ansible

Los frontales web necesitan una preparación previa antes de ejecutar los playbooks de Ansible.
Al ser una imagen que distribuye Nexica no está preparada para nuestras necesidades.

Para hacer el setup inicial debemos ejecutar los siguientes comandos:

  - Accedemos al servidor: ssh admin@x.x.x.x (Debemos pedirle a Nexica que no cambien la contraseña inicial. Es "template")
  - Crear el usuario que utilizaremos desde los playbooks: sudo adduser -u 1100 -G wheel aba
  - Ponemos una contraseña a ese usuario: sudo passwd aba
  - (Ejecutar desde "logger") Copiamos la key ssh para automatizar el proceso: ssh-copy-id aba@x.x.x.x

Finalmente sólo nos tenemos que asegurar que nuestro servidor está en el catálogo de Ansible.
### Deployment

Cuando tenemos el servidor con el setup inicial ya podemos desplegar el template web de la siguiente forma:

```
$ ansible-playbook -i production web.yml --limit campuswebXX -K <-- Siendo XX el número de nodo y -K para indicarle la contraseña del usuario "aba"
```

Una vez aplicado todo el rol lo recomendable es reiniciar el nodo para asegurarnos que todo arranque automáticamente.
Cuando el servidor haya arrancado ya podemos hacer el deploy de abawebapps y transactions mirando la sección de Deploy de este documento.

----
### Nexica Actions

Los últimos pasos ya dependen de Nexica:

  - Solicitar que hagan un NAT de una ip pública a la ip interna del servidor
     - Por motivos de seguridad a Nexica hay que decirle que este nodo sólo reciba peticiones de los balanceadors por el 80, no debe estar expuestos a Internet directamente
  - Solicitar que añadan el nuevo nodo al balanceador

----
### Trucos

En el servidor logger hay ciertos trucos y/o atajos que merece su mención para agilizar al máximo la gestión

  - Acceder a los logs: Ejecutar "logs" y accederemos directamente a /var/log/abalogs
  - Los logs están ordenados por AÑO/MES/DIA pero hay una variable en logger llamada $hoy que te devuelve ese mismo formato de la fecha actual, una vez dentro de abalogs
    y accedáis a servers o services podréis hacer un "cd $hoy"
  - Desde el propio logger podréis ver el server-status de los nodos con un simple 'ssh campusweb0X "apache_dumper.py"' <- Cambiando la X por el número de nodo
  - HAProxy se puede consultar con http://logger:1936 o bien desde la línea de comandos "sudo hatop -s /var/lib/haproxy/stats"
  - Si queréis hacer deploy automáticamente sólo tendréis que poner contraseña al usuario wwwuser y crear un playbook con las sentencias git necesarias

----
### Deploy de abawebapps y transactions

Nos encontramos con dos tipos de deploy en este configuration management.
Tenemos el deploy de transactions que se ejecuta a través de un rol simple que está dentro de la carpeta roles y el deploy de abawebapps que se utiliza la herramienta de ansistrano.

== Transactions ==

El deploy se ejecuta de la siguiente forma:

  - En la raíz de config-manager ejecutar: 
  
  ```
  ansible-playbook -i production deploy.yml -e "trx=1.4.6" 
  ``` 

== ABAWebapps ==

Este deploy es más complejo y lento pero más seguro al disponer de un rol de deploy y otro de rollback basado en git + symlink de la carpeta current a la release actual.
El montaje está en una carpeta llamada deploy dentro de config-manager y se ejecuta de la siguiente forma:

   - En la raíz de config-manager ejecutar: 
  
   ```
  ansible-playbook deploy/deploy.yml -i deploy/hosts/prod -e "version=feature/loggly"
  ```
  
Siempre se hace el deploy del curso, eso quiere decir que en cada deploy se ejecuta el composer install, es una limitación de la herramienta al montar releases nuevas cada vez.

En el caso que queramos hacer un rollback hay que ejecutar lo siguiente:

   - En la raíz de config-manager ejecutar: 
   
   ```
  ansible-playbook deploy/rollback.yml -i deploy/hosts/prod
  ```
  

