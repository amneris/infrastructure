aws kms decrypt --ciphertext-blob fileb://roles/secrets/templates/secret-encrypt-key.yml.j2.encrypted --query Plaintext --output text | base64 --decode > roles/secrets/templates/secret-encrypt-key.yml.j2;
aws kms decrypt --ciphertext-blob fileb://roles/secrets/templates/nexus-secret.yml.j2.encrypted --query Plaintext --output text | base64 --decode > roles/secrets/templates/nexus-secret.yml.j2;
aws kms decrypt --ciphertext-blob fileb://roles/secrets/templates/secret-mysql.yml.j2.encrypted --query Plaintext --output text | base64 --decode > roles/secrets/templates/secret-mysql.yml.j2;
