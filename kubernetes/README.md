# Kubernetes deploy

This folder contains a series of ansible playbooks to automate and simplify deployments in kubernetes environments. You can see more in-depth information about [kubernetes](http://kubernetes.io/docs/user-guide/walkthrough/) and [ansible](http://docs.ansible.com/ansible/)

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Deprecation Warning!](#deprecation-warning)
- [Requeriments](#requeriments)
- [Usage](#usage)
- [Directory Layout](#directory-layout)
- [Known Issues](#known-issues)
  - [Deployment health check](#deployment-health-check)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Deprecation Warning!

This folder is being changed to contain only playbooks and roles, and will be changed to be inside /ansible/kubernetes folder in a mid-term.

In any case, this changes will try to remain being backward compatible until final change.

Currently, the only working environment in the new way is minikube, although still works as usally did. It can be executed with the following commands:

```
/ansible $ ansible-playbook -i local.inv kubernetes/site.yml
or
/kubernetes $ ansible-playbook -i minikube site.yml
```

The following sections will remain outdated.

## Requeriments

- Clone this repository
    - git clone git@github.com:abaenglish/infrastructure
- Update brew repository
    - brew update
- Install awscli, kubectl and ansible
    - brew install awscli, kubectl, ansible
- AWS account with decrypt permissions (if you have configured AWS with abaland credentials, skip this step)
    - ask to imorato and you shall receive

## Usage

To install all ABA English platform on a brand new environment you must execute the following command:

```
$ ansible-playbook -i <environment> site.yml
```

If you want to update some service version you can execute:

```
$ ansible-playbook -i <environment> <service-name>.yml -e image_version=<image version>
```

## Directory Layout

Extracted from the [ansible best practices](http://docs.ansible.com/ansible/playbooks_best_practices.html#directory-layout) we use the following layout:

```
minikube                    # inventory file for local server
dev                         # inventory file for development server
qa                          # inventory file for qa server
pro                         # inventory file for production server

site.yml                    # master playbook
pre-site.yml                # playbook for previous configuration
backing.yml                 # playbook for backing services tier (mysql, rabbit...)
backend.yml                 # playbook for backend services tier
frontend.yml                # playbook for frontend services tier
post-site.yml               # playbook for post configuration
subscription-service.yml    # playbook for subscription service
aba-moments.yml             # playbook for aba-moments
payment-funnel.yml          # playbook for payment funnel
....yml                     # all root yaml files are playbooks

roles/
    backend-services/       # this hierarchy represents a "role"
        tasks/              #
            main.yml        #  <-- tasks file can include smaller files if warranted
        handlers/           #
            main.yml        #  <-- handlers file
        templates/          #  <-- files for use with the template resource
            ntp.conf.j2     #  <------- templates end in .j2
        files/              #
            bar.txt         #  <-- files for use with the copy resource
            foo.sh          #  <-- script files for use with the script resource
        vars/               #
            main.yml        #  <-- variables associated with this role
        defaults/           #
            main.yml        #  <-- default lower priority variables for this role
        meta/               #
            main.yml        #  <-- role dependencies

    frontend-services/      # same kind of structure as "backend-services" was above
    fluentd/                # ""
    redis/                  # ""
```

## Known Issues

### Deployment health check
When the cluster is started site.yml playbook will wait for each service to be deployed before the execution of the next step, executing a health check:
```shell
$ kubectl get deployment service-name --template={{.status.availableReplicas}}
```
When this command returns 1 it means that the service is running. When a services is deployed individually via ```ansible-playbook -i minikube subscription-service.yml``` the health check command will returns 1 instantly because a pod is already running.
We need to find a way to be compatible with these two behaviors.
